Welcome to The Javascript Tools Guide
=====================================

.. toctree::
    :maxdepth: 2
    :caption: Chapter 1 - Introduction

    1 - Introduction/index
    1 - Introduction/extendscript-overview
    1 - Introduction/scripting-for-specific-applications


.. toctree::
    :maxdepth: 2
    :caption: Chapter 2 - The ExtendScript Toolkit

    2 - ExtendScript Toolkit/index
    2 - ExtendScript Toolkit/configuring-the-toolkit-window
    2 - ExtendScript Toolkit/selecting-scripts
    2 - ExtendScript Toolkit/the-script-editor
    2 - ExtendScript Toolkit/debugging-in-the-toolkit
    2 - ExtendScript Toolkit/code-profiling-for-optimization
    2 - ExtendScript Toolkit/inspecting-object-models

.. toctree::
    :maxdepth: 2
    :caption: Chapter 3 - File System Access

    3 - File System Access/index
    3 - File System Access/using-file-and-folder-objects
    3 - File System Access/file-access-error-messages
    3 - File System Access/file-and-folder-supported-encoding-names
    3 - File System Access/file-object
    3 - File System Access/folder-object

.. toctree::
    :maxdepth: 2
    :caption: Chapter 4 - User-Interface Tools

    4 - User-Interface Tools/index
    4 - User-Interface Tools/code-examples-for-scriptui
    4 - User-Interface Tools/scriptui-programming-model
    4 - User-Interface Tools/types-of-controls
    4 - User-Interface Tools/size-and-location-objects
    4 - User-Interface Tools/drawing-objects
    4 - User-Interface Tools/resource-specifications
    4 - User-Interface Tools/defining-behavior-with-event-callbacks-and-listeners
    4 - User-Interface Tools/communicating-with-the-flash-application
    4 - User-Interface Tools/automatic-layout
    4 - User-Interface Tools/managing-control-titles
    4 - User-Interface Tools/localization-in-scriptui-objects
    4 - User-Interface Tools/scriptui-object-reference
    4 - User-Interface Tools/scriptui-class
    4 - User-Interface Tools/common-properties
    4 - User-Interface Tools/window-class
    4 - User-Interface Tools/window-object
    4 - User-Interface Tools/control-objects
    4 - User-Interface Tools/event-handling
    4 - User-Interface Tools/graphic-customization-objects
    4 - User-Interface Tools/layoutmanager-object


.. toctree::
    :maxdepth: 2
    :caption: Chapter 5 - Interapplication Communication with Scripts

    5 - Interapplication Communication with Scripts/index
    5 - Interapplication Communication with Scripts/communications-overview
    5 - Interapplication Communication with Scripts/cross-dom-functions
    5 - Interapplication Communication with Scripts/communicating-through-messages
    5 - Interapplication Communication with Scripts/messaging-framework-api-reference
    5 - Interapplication Communication with Scripts/bridgetalk-class
    5 - Interapplication Communication with Scripts/bridgetalk-message-object
    5 - Interapplication Communication with Scripts/messaging-error-codes
    5 - Interapplication Communication with Scripts/application-and-namespace-specifiers

.. toctree::
    :maxdepth: 2
    :caption: Chapter 6 - External Communication Tools

    6 - External Communication Tools/index
    6 - External Communication Tools/socket-object
    6 - External Communication Tools/socket-object-reference


.. toctree::
    :maxdepth: 2
    :caption: Chapter 7 - Integrating External Libraries

    7 - Integrating External Libraries/index
    7 - Integrating External Libraries/loading-and-using-shared-libraries
    7 - Integrating External Libraries/externalobject-object
    7 - Integrating External Libraries/defining-entry-points-for-direct-access
    7 - Integrating External Libraries/defining-entry-points-for-indirect-access

.. toctree::
    :maxdepth: 2
    :caption: Chapter 8 - ExtendScript Tools and Features

    8 - ExtendScript Tools and Features/index
    8 - ExtendScript Tools and Features/dollar-object
    8 - ExtendScript Tools and Features/extendscript-reflection-interface
    8 - ExtendScript Tools and Features/localizing-extendscript-strings
    8 - ExtendScript Tools and Features/user-notification-dialogs
    8 - ExtendScript Tools and Features/specifying-measurement-values
    8 - ExtendScript Tools and Features/preprocessor-directives
    8 - ExtendScript Tools and Features/operator-overloading


.. toctree::
    :maxdepth: 2
    :caption: Chapter 9 - Integrating XML into JavaScript

    9 - Integrating XML into JavaScript/index
    9 - Integrating XML into JavaScript/the-xml-object
    9 - Integrating XML into JavaScript/xml-object-reference


.. toctree::
    :maxdepth: 2
    :caption: Chapter 10 - Scripting Access to XMP Metadata

    10 - Scripting Access to XMP Metadata/index
    10 - Scripting Access to XMP Metadata/accessing-the-xmp-scripting-api
    10 - Scripting Access to XMP Metadata/xmpscript-object-reference

.. toctree::
    :maxdepth: 2
    :caption: Chapter 11 - Porting Guide

    11 - Porting Guide/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
