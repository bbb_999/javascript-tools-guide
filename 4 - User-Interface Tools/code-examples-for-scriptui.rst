.. _code-examples-for-scriptui:

Code examples for ScriptUI
==========================
The sample code distributed with the Adobe ExtendScript SDK includes code examples that specifically
demonstrate different ways of building and populating a ScriptUI dialog.

=============================== ==========================================================================================
Building ScriptUI dialogs
=============================== ==========================================================================================
SnpCreateDialog.jsx             Creates a very simple, modeless dialog (a palette) with OK
                                and Cancel button behavior.
SnpCreateUIAddMethod.jsx        Shows how to add controls to a dialog using the add
                                method.
SnpCreateUIResourceSpec.jsx     Shows how to define a resource string that creates the
                                control hierarchy in a dialog.
SnpCreateTreeView.jsx           Shows how to create a hierarchical list with subitems.
SnpCreateProgressBar.jsx        Shows how to create, initialize, and update a progress bar.
SnpCreateSlider.jsx             Shows how to create and handle input from a slider control.
UsingFlashPlayer.jsx            Shows how to create a Flash® Player, and use it to load a play
                                back a movie defined in an SWF file.
ActionScriptDemo.jsx            Shows how to communicate between the Adobe
                                application scripting environment and the ActionScript™
                                scripting environment of the Flash Player.
ColorSelector.jsx               Shows how to use the graphics objects to change colors in a
                                window.
ColorPicker.jsx                 A more complex version of the color-selection dialog shows
                                how to use additional graphics objects, including fonts and
                                paths.
SnpAlignElements.jsx            Shows how to align elements along two dimensions in order
                                to control the relative positions of controls within rows and
                                columns.
SnpCreateDynamicScriptUI.jsx    Shows how to use automatic layout, switching component
                                layout between "row" and "stack" orientation.
AlertBoxBuilder1.jsx            Shows a way to use resource specifications. Uses the add()
                                method to build a dialog that collects values from the user,
                                and creates a resource string from those values. Saves the
                                string to a file, then uses it to build a new dialog. See :ref:`using-resource-strings`.
AlertBoxBuilder2.jsx            Shows another way to use a resource specification, building
                                the same user-input dialog itself from a resource string. See
                                :ref:`using-resource-strings`.
SnpCustomLayoutManager.jsx      Shows how to create a customized layout manager. See
                                :ref:`custom-layout-manager-example`.
=============================== ==========================================================================================
